#!/bin/sh
WORKGROUP=${WORKGROUP_NAME:-Development}
USER=pandoc

cat /data/samba.conf.template | envsubst > /etc/samba/smb.conf
cat /data/shellinabox.supervisord | envsubst > /etc/supervisor/conf.d/shellinabox.conf

exec supervisord -n -c /etc/supervisor/supervisord.conf


