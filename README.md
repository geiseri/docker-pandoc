# Pandoc Docker Image

A docker image for running the latest version of [Pandoc](https://github.com/jgm/pandoc).  
This provides a html5 console to the docker (port 4200), and exposes the "work" directory over CIFS.  
The "work" directory is completely volatile so all changes will be lost when the container exits.

## Usage


```bash
sudo ./runpandoc <tag> [windows workgroup] [bridge interface]
```

