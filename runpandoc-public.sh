#!/bin/sh -e

TAG="$1"
WORKGROUP="${2:-development}"
IFACE="${3:-br0}"

DOC_ID=$(docker run -d -h pandoc --net=none -e WORKGROUP="${WORKGROUP}" -e USER_ID="pandoc" -v /home/oe:/host $TAG)

echo "Started: ${DOC_ID}"
sudo ./pipework ${IFACE} ${DOC_ID} dhcp

CONTAINER_IP=$(docker exec -it ${DOC_ID} ip -o -4 addr show | awk -F '[ /]+' '/global/ {print $4}' | head -n1)

echo "========================================================================"
echo "You can now connect to this pandoc container:"
echo ""
echo " Console: http://${CONTAINER_IP}:4200/"
echo " Windows Volatile Share: \\\\${CONTAINER_IP}\WorkShared"
echo " Windows Host Share: \\\\${CONTAINER_IP}\HostShared"
echo ""
echo "========================================================================"


