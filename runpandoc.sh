#!/bin/sh
TAG=$1
SHELL=${2:-1022}
SMB=${3:-1139}

docker run -d  -name "pandoc-run" -p ${SHELL}:4200 -p ${SMB}:139 -v $HOME:/Shared:rw ${TAG}

IP=$(docker exec ${TAG}-run /bin/ip route | awk '/default/ {print $9}')

echo "========================================================================"
echo "You can now connect to this pandoc container:"
echo ""
echo " Console: http://${IP}:${SHELL}/"
echo " Windows Host Share: \\\\${IP}:${SMB}\Shared"
echo ""
echo "========================================================================"


