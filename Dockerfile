FROM ubuntu
MAINTAINER Ian Geiser <geiseri@yahoo.com>

#RUN sed -i 's|http[a-zA-Z0-9:/\.]\+|mirror://mirrors.ubuntu.com/mirrors.txt|g' /etc/apt/sources.list

# configure locale
RUN locale-gen --no-purge en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8  
ENV LANGUAGE en_US:en
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -yq update
RUN apt-get -yq upgrade

# Define working directory.
WORKDIR /data

# Define shared host directory
VOLUME /Shared

RUN apt-get install -qy supervisor

RUN apt-get install -qy curl wget git \
                        make nano 
                        
# install pandoc deps
RUN apt-get install -qy texlive texlive-latex-base \
                        texlive-xetex latex-xcolor \
                        texlive-math-extra texlive-latex-extra \
                        texlive-fonts-extra fontconfig

# install fonts
RUN apt-get install -qy fonts-dejavu \
                        fonts-liberation  

# install pandoc
RUN wget -O /tmp/pandoc.deb https://github.com/jgm/pandoc/releases/download/1.13.2/pandoc-1.13.2-1-amd64.deb && \
    dpkg --install /tmp/pandoc.deb

# install python
RUN apt-get install -qy python

# install filters
RUN cd /tmp/ && \
    git clone https://github.com/jgm/pandocfilters.git && \
    cd pandocfilters && \
    python setup.py install

# install login shell
RUN wget -O /tmp/shellinabox.deb https://github.com/anilgulecha/shellinabox/releases/download/v2.14-5/shellinabox_2.14-5_amd64.deb_ && \
    dpkg --install /tmp/shellinabox.deb || true
    
RUN apt-get install -qyf

ADD shellinabox.supervisord /data/shellinabox.supervisord

# install samba share
RUN apt-get install -qy samba gettext
ADD samba.conf.template /data/samba.conf.template
ADD samba.supervisord /etc/supervisor/conf.d/samba.conf

# install inkscape for svg conversions
RUN apt-get install -qy inkscape

# expose ports
EXPOSE 4200
EXPOSE 139

# configure user
RUN useradd -u 1000 -m --shell /bin/bash pandoc

# configure entrypoint
ADD init.sh /data/init.sh
RUN chmod 755 /data/init.sh
ENTRYPOINT ["/data/init.sh"]

# cleanup
RUN apt-get -yq autoremove && \
    apt-get clean -y && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
